import {getToken} from '@/utils/auth.js';
import store from "@/store/index.js";
import Vue from 'vue';


/**
 * @description: 校验是否登录
 * @param: String
 * @returns: Promise
 */

export function noTokenToLogin(flag = true) {
	return new Promise((resolve, reject) => {
		// 未登录过
		if (!getToken() && (Object.keys(store.state.userInfo).length == 0)) {
			uni.showModal({
				title: '提示',
				content: '您还未登录, 前往登录?',
				confirmColor: Vue.prototype.appPrimaryColor,
				success: function (res) {
					if (res.confirm) {
						uni.navigateTo({
							url: '/pageDiamond1/login/index'
						});
						if(flag) {
							reject();
						} else {
							resolve();
						}
						
					}
				}
			})
		} else {
			if(flag) {
				resolve();
			} else {
				reject();
			}
		}
	})
}

/**
 * @description: 跳转前判断
 * @param: String
 * @returns: Promise
 */

export function cuNavigateTo(url = '', isToken = true) { // 默认是navigateTo方式
	if(isToken) {
		noTokenToLogin().then(() => {
			uni.navigateTo({
				url
			})
		});
	} else {
		uni.navigateTo({
			url
		})
	}
};

export function formatTime(time, fmt) {
    if (!time) return '';
    else {
        const date = new Date(time);
        const o = {
            'M+': date.getMonth() + 1,
            'd+': date.getDate(),
            'H+': date.getHours(),
            'm+': date.getMinutes(),
            's+': date.getSeconds(),
            'q+': Math.floor((date.getMonth() + 3) / 3),
            S: date.getMilliseconds(),
        };
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(
                RegExp.$1,
                (date.getFullYear() + '').substr(4 - RegExp.$1.length)
            );
        for (const k in o) {
            if (new RegExp('(' + k + ')').test(fmt)) {
                fmt = fmt.replace(
                    RegExp.$1,
                    RegExp.$1.length === 1
                        ? o[k]
                        : ('00' + o[k]).substr(('' + o[k]).length)
                );
            }
        }
        return fmt;
    }
}

/**
 * @param {Number} num 待处理数组
 * @param {Number} num1 待处理数组
 * @param {String} format 待处理数组
 */
export function dateGet(num, num1, format = 'MM/dd') {
    let weekday = ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'];
    // 1 6
    let dateTime = [];
    let strDate = [];
    let curTime = new Date().getTime();
    // 前
    for (let i = 0; i < num; i++) {
        dateTime.push(curTime - 3600 * 24 * 1000 * (i + 1))
    }
    // 后
    for (let i = 0; i < num1 - num; i++) {
        dateTime.push(curTime + 3600 * 24 * 1000 * i)
    }
    dateTime.forEach(item => {
        strDate.push({
            date: formatTime(item, format),
            week: weekday[new Date(item).getDay()]
        });
    })
    return strDate
}

/**
 * @param {Number} num 待处理数组
 * @param {Number} num1 待处理数组
 * @param {String} format 待处理数组
 */
export function chunkArr(arr, size) {
	return Array.from({
    length: Math.ceil(arr.length / size)
}, (v, i) => arr.slice(i * size, i * size + size))
}


export function hexToRGB(hex){
  let alpha = false,
    h = hex.slice(hex.startsWith('#') ? 1 : 0);
  if (h.length === 3) h = [...h].map(x => x + x).join('');
  else if (h.length === 8) alpha = true;
  h = parseInt(h, 16);
  return (
    'rgb' +
    (alpha ? 'a' : '') +
    '(' +
    (h >>> (alpha ? 24 : 16)) +
    ', ' +
    ((h & (alpha ? 0x00ff0000 : 0x00ff00)) >>> (alpha ? 16 : 8)) +
    ', ' +
    ((h & (alpha ? 0x0000ff00 : 0x0000ff)) >>> (alpha ? 8 : 0)) +
    (alpha ? `, 0.${h & 0x000000ff}` : '') +
    ')'
  );
};

export function mapGetCenter(pointArray) {
  var sortedLongitudeArray = pointArray.map(item => item[1]).sort();//首先对经度进行排序，红色部分是array中经度的名称
  var sortedLatitudeArray = pointArray.map(item => item[0]).sort();//对纬度进行排序，红色部分是array中纬度的名称
  var centerLongitude = ((parseFloat(sortedLongitudeArray[0]) + parseFloat(sortedLongitudeArray[sortedLongitudeArray.length - 1])) / 2).toFixed(4);
  const centerLatitude = ((parseFloat(sortedLatitudeArray[0]) + parseFloat(sortedLatitudeArray[sortedLatitudeArray.length - 1])) / 2).toFixed(4);

  //如果经纬度在array中不是数字类型，需要转化为数字类型进行计算，如果是可以去掉parseFloat处理
  //console.log(centerLongitude+"kkk"+centerLatitude);
  return [centerLatitude, centerLongitude];
}

export function timeFormat(dateTime = null, formatStr = 'yyyy-mm-dd') {
	  let date
		// 若传入时间为假值，则取当前时间
	  if (!dateTime) {
	    date = new Date()
	  }
	  // 若为unix秒时间戳，则转为毫秒时间戳（逻辑有点奇怪，但不敢改，以保证历史兼容）
	  else if (/^\d{10}$/.test(dateTime?.toString().trim())) {
	    date = new Date(dateTime * 1000)
	  }
	  // 若用户传入字符串格式时间戳，new Date无法解析，需做兼容
	  else if (typeof dateTime === 'string' && /^\d+$/.test(dateTime.trim())) {
	    date = new Date(Number(dateTime))
	  }
	  // 其他都认为符合 RFC 2822 规范
	  else {
	    // 处理平台性差异，在Safari/Webkit中，new Date仅支持/作为分割符的字符串时间
	    date = new Date(
	      typeof dateTime === 'string'
	        ? dateTime.replace(/-/g, '/')
	        : dateTime
	    )
	  }
	
		const timeSource = {
			'y': date.getFullYear().toString(), // 年
			'm': (date.getMonth() + 1).toString().padStart(2, '0'), // 月
			'd': date.getDate().toString().padStart(2, '0'), // 日
			'h': date.getHours().toString().padStart(2, '0'), // 时
			'M': date.getMinutes().toString().padStart(2, '0'), // 分
			's': date.getSeconds().toString().padStart(2, '0') // 秒
			// 有其他格式化字符需求可以继续添加，必须转化成字符串
		}
	
	  for (const key in timeSource) {
	    const [ret] = new RegExp(`${key}+`).exec(formatStr) || []
	    if (ret) {
	      // 年可能只需展示两位
	      const beginIndex = key === 'y' && ret.length === 2 ? 2 : 0
	      formatStr = formatStr.replace(ret, timeSource[key].slice(beginIndex))
	    }
	  }
	
	  return formatStr
	}